rm -rf .git
git init
rm -rf bundle/*

rm -rf bundle/tlib_vim 
git submodule add https://github.com/tomtom/tlib_vim.git bundle/tlib_vim

rm -rf bundle/vim-airline
git submodule add https://github.com/bling/vim-airline  bundle/vim-airline

rm -rf bundle/tagbar
git submodule add https://github.com/majutsushi/tagbar.git bundle/tagbar

rm -rf bundle/vim-snippets
git submodule add https://github.com/garbas/vim-snipmate.git bundle/vim-snipmates

rm -rf bundle/vim-snippets
git submodule add https://github.com/honza/vim-snippets.git bundle/vim-snippets

# GIT 
rm -rf bundle/gitgutter
git submodule add git://github.com/airblade/vim-gitgutter.git bundle/vim-gitgutter

rm -rf bundle/vim-javascript
git submodule add https://github.com/pangloss/vim-javascript.git bundle/vim-javascript


rm -rf bundle/vim-addon-mw-utils/
git submodule  add https://github.com/MarcWeber/vim-addon-mw-utils.git bundle/vim-addon-mw-utils



rm -rf bundle/nerdtree
git submodule add https://github.com/scrooloose/nerdtree.git bundle/nerdtree

rm -rf bundle/supertab
git submodule add https://github.com/ervandew/supertab.git bundle/supertab


